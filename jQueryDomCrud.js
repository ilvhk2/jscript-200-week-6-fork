//1- Wrap your JS code in the jQuery document.ready method
$(document).ready(function() {       
    //4- Create a new <a> element containing the text "Buy Now!"
    // with an id of "cta" after the last <p>
    let $newA = $('<a>');
    $newA.text("Buy Now!");
    $newA.attr('id','cta');
       // $("<a id='cta'>Buy Now!</a>").insertAfter("p");
       //  $("<a>Buy Now!</a>").insertAfter("p");
       // $newA.appendTo('main')
    $newA.insertAfter("p");

    //2- Access (read) the data-color attribute of the <img>,
    // log to the console
    
    //console.log ( $('img').data("color")  );
    const img = $('img')
    console.log(img.attr('data-color'));

    //3- Update the third <li> item ("Turbocharged"),
    // set the class name to "highlight"  
    $('li').eq(2).addClass('highlight');

    //5- Remove (delete) the last paragraph
    // (starts with "Available for purchase now…")
    $('p').last().remove();

    //6- Create a listener on the "Buy Now!" link that responds to a click event.
    //  When clicked, the "Buy Now!" link should be removed and replaced with text that says "Added to cart"
    $('a#cta').on('click', function (e) {
        $(this).html("Added to cart");
    })

    // $('#cta').click( function(e){
    //     console.log(e)
    //     $(this).remove();
    //     const $p = $('<p>');
    //     $p.text("Added to cart");
    //     $('main').append($p);
    // });

    $('ul').on('click', 'li',  function () {
        console.log(this);
        const txt = $(this).html();
       // $(this).html( txt + "    added to cart");
        alert( txt + " has added to cart"  );
    });

})