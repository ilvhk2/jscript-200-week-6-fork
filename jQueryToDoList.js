$(document).ready( function(){
  $('body').css('backgroundColor','lightgreen');
  /**
   * Toggles "done" class on <li> element
   */

    // $('ul').on('click', 'li',  function () {
    //   //  console.log(this);
    //    $(this).toggleClass("done");
    // });
  $('li').click( function(){
      $(this).toggleClass("done");
  })

  /**
   * Delete element when delete link clicked
   */
  $('a.delete').click(function(e){
    e.stopPropagation();
     // console.dir( 'removed : ' + $(this).parent().html() );
      //$(this).parent().remove();
      $(this).parent().fadeOut(3000);
  })

  /**
   * Adds new list item to <ul>
   */
  const addListItem = function(e) {
    // e.preventDefault(); 
    const text = $('input').val();
    // rest here...
    if (text.length){
          const txt ='<span>'+ text + '</span><a class="delete"> Delete </a>';
          $('<li />',{html:txt})
          .appendTo('ul.today-list')
          .click( function(){
               $(this).toggleClass("done");
          })
          $("a.delete").click(function(e){
            e.stopPropagation();
            console.dir( 'removed : ' + $(this).parent().html() );
           // $(this).parent().remove();
           $(this).closest('li').fadeOut(3000);
          })
    } else {
        alert('Please enter text in the box')
    }
  };

  // add listener for add
  $("div.add a.add-item").on('click', function(){
      addListItem();
      $('input').val('');
  })   
})


